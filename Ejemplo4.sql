﻿                                            /* EJEMPLO 4 */

/* Ejercicio 1: Crea un procedimiento que muestre en pantalla el texto "esto es un ejemplo de procedimiento"
                (donde la cabecera de la columna sea "mensaje") */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej4_1()
BEGIN
  SELECT 'esto es un ejemplo de procedimiento' mensaje;
END //
DELIMITER ;

CALL ej4_1();


/* Ejercicio 2: Crea un procedimiento donde se declaren tres variables internas, una de tipo entero con valor de 1,
                otra de tipo varchar(10) con valor nulo por defecto y otra de tipo decimal con 4 dígitos
                y dos decimales con valor 10,48 por defecto */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej4_2()
BEGIN

  DECLARE v1 int DEFAULT 1;
  DECLARE v2 varchar(10) DEFAULT NULL;
  DECLARE v3 float(4,2) DEFAULT 10.48;

  SELECT v1,v2,v3;
END //
DELIMITER ;

CALL ej4_2();


/* Ejercicio 3: Copiar, comentar y corregir los errores en el siguiente codigo */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej4_3()
BEGIN
  /* variables */
  DECLARE v_caracter1 char(1);  -- un solo caracter
  DECLARE forma_pago enum('metalico','tarjeta','transferencia'); -- lista 
  /* cursores */
  /* control excepciones */
  /* programa */
  SET v_caracter1 = 'hola';
  SET forma_pago = 1;
  SET forma_pago = 'cheque'; 
  SET forma_pago = 4;
  SET forma_pago = 'TARJETA';
  SELECT forma_pago;
END //
DELIMITER ;

CALL ej4_3();

 -- CORRECCIONES
DELIMITER //
CREATE OR REPLACE PROCEDURE ej4_3ok()
BEGIN
  /* variables */
  DECLARE v_caracter1 char(1);  
  DECLARE forma_pago enum('metalico','tarjeta','transferencia'); -- lista 
  /* cursores */
  /* control excepciones */
  /* programa */


  SET v_caracter1 = 'hola'; -- AL MENOS NECESITAMOS 4 CARACTERES PARA LA PALABRA 'hola'
  SELECT v_caracter1;
  SET forma_pago = 1; -- selecciona la PRIMERA posicion de una lista predeterminada
  SELECT forma_pago;
  SET forma_pago = 'cheque'; -- no permite una palabra que no este en la lista
  SELECT forma_pago;
  SET forma_pago = 4; -- no permite numero un numero mayor que el numero de registros que hay en la lista
  SELECT forma_pago;
  SET forma_pago = 'TARJETA';  -- aunque en mayusculas, identifica el texto pero lo muestra como esta predefinido
  SELECT forma_pago;
 
END //
DELIMITER ;

CALL ej4_3ok();
SELECT * FROM ej4_3ok;

/* Ejercicio 4: Escribe procedimiento que reciba un parametro de entrada (numero real) y muestre el numero en pantalla. */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej4_4(IN numero int)
BEGIN
  SELECT numero;
END //
DELIMITER ;

CALL ej4_4(5);
CALL ej4_4(100);
CALL ej4_4(35);


/* Ejercicio 5: Escribe procedimiento que reciba un parametro de entrada (numero real) y
                asigne ese parametro a una variable interna del mismo tipo.
                Despues mostrar la variable en pantalla. */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej4_5(IN numero int)
BEGIN
  DECLARE v1 int;
    SET v1=numero;
  SELECT v1;
END //
DELIMITER ;

CALL ej4_5(35);
CALL ej4_5(170);
CALL ej4_5(35245);

/* Ejercicio 6: Escribe procedimiento que reciba un numero real de entrada y muestre un mensaje indicando
                si el numero es positivo, negativo o cero */

DELIMITER //
CREATE OR REPLACE PROCEDURE ej4_6(IN numero int)
BEGIN
  DECLARE v1 varchar(10);
  
  CASE
    WHEN numero<0  THEN 
      SET v1='negativo';
    WHEN numero>0 THEN 
      SET v1='positivo';
    ELSE 
      SET v1='cero';
  END CASE;
  

  SELECT v1;
END //
DELIMITER ;

CALL ej4_6(5);
CALL ej4_6(-4);
CALL ej4_6(0);

/* Ejercicio 7: Escribe una funcion que
  */
